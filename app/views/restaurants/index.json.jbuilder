# json.array! @restaurants, partial: 'restaurant', as: :restaurant

json.status 'success'
json.data do
  json.array! @restaurants do |restaurant|
    json.(restaurant, :id, :name, :description, :locations, :review, :tags, :rate)
    json.avatar restaurant.avatar_url
    json.city restaurant.locations.first.city
    json.latitude restaurant.locations.first.latitude
    json.longitude restaurant.locations.first.longitude
  end
end