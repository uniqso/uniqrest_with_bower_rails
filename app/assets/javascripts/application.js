// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// jQuery is controlled by bower
// require jquery
// require jquery_ujs
//
//= require angular
//= require angular-animate
//= require angular-aria
//= require angular-flash
//= require angular-google-maps
//= require angular-material
//= require angular-mocks
//= require angular-resource
//= require angular-route
//= require angular-rails-templates
//
//= require jquery
//= require geolib
//= require lodash/lodash
// require materialize
//= require bootstrap-sass

//= require_tree ./templates
//= require_tree .
