ActiveAdmin.register Restaurant do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model

  permit_params :name, :description, :url, :review, :avatar, :remote_avatar_url, :tags, locations: [], ratings: []
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  sidebar "Restaurants properties", only: [:show, :edit] do
    ul do
      li link_to "Locations", admin_restaurant_locations_path(restaurant)
      li link_to "Ratings", admin_restaurant_ratings_path(restaurant)
    end
  end

end
