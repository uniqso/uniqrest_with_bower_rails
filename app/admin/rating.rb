ActiveAdmin.register Rating do
  belongs_to :restaurant
  navigation_menu :restaurant

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :restaurant_id, :rating_criterion_id, :score

  index do
    column :id
    column :rating_criterion
    column :score
    column :updated_at
    actions
  end

end
