class RestaurantsController < ApplicationController
  def index
    @restaurants = Restaurant.all
    @restaurants = @restaurants.select { |restaurant| restaurant.locations.count > 0 }
  end
end
