class Restaurant < ActiveRecord::Base
  has_many :locations, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :rating_criteria, :through => :ratings

  attr_reader :rate

  mount_uploader :avatar, AvatarUploader

  def rate
    if self.ratings.count > 0
      rate = 0
      self.ratings.each do |rating|
        rate += rating.score
      end
      rate /= self.ratings.count
    else
      rate = 'n/a'
    end
  end
end
