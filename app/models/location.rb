class Location < ActiveRecord::Base
  belongs_to :restaurant

  geocoded_by :full_street_address
  after_validation :geocode

  def full_street_address
    [ self.address, self.city, self.country ].join(', ')
  end
end
