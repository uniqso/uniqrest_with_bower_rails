class Rating < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :rating_criterion

  validates_uniqueness_of :rating_criterion_id, scope: [:restaurant_id]
  validates :score, numericality: { greater_than_or_equal_to: 0,
                                    less_than_or_equal_to: 5 }
end
