class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.references :restaurant, index: true, foreign_key: true
      t.string :country
      t.string :city
      t.string :address
      t.float :latitude
      t.float :longitude

      t.timestamps null: false
    end
  end
end
