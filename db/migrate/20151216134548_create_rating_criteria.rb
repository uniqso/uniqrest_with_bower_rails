class CreateRatingCriteria < ActiveRecord::Migration
  def change
    create_table :rating_criteria do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
