class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.references :restaurant, index: true, foreign_key: true
      t.references :rating_criterion, index: true, foreign_key: true
      t.float :score

      t.timestamps null: false
    end
  end
end
